#Funcion palindromo
def palindromo(str):
	for i in range(0, int(len(str)/2)):
		printedVar = (str[i] + " == " + str[len(str) - 1 - i])
		if str[i] != str[len(str) - 1 - i]:
			printedVar = printedVar + " is false"
			return False
		else:
			printedVar = printedVar + " is true"
		print(printedVar)
	return True

assert(palindromo("ollo") == True)
assert(palindromo("hello") == False)
